// Our internal crates
mod core;
mod memory;

struct EMPDV {
    core: core::Core,
}

impl EMPDV {
     pub fn new() -> Self {
         EMPDV {
             core: core::Core::new(),
         }
     }
}

fn main() {
    let empdv = EMPDV::new();
}
