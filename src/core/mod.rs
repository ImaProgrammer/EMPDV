pub struct Core {
	pub gpr: [u64; 64],	// General Purpose Registers
	pub fpr: [f64; 64],	// Floating-Point Registers
	pub pc: u64,		// Program Counter
	pub ir: u64,		// Instruction Register
	pub sp: u64,		// Stack Pointer
}

impl Core {
	pub fn new() -> Self {
		Core {
			gpr: [0; 64],
			fpr: [0.0; 64],
			pc: 0,
			ir: 0,
			sp: 0,
		}
	}
}
